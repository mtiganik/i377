package model;

public class ValidationErrorChecker {

    public static ValidationError CheckOrderErrors(Order order){
        if(order.getOrderNumber().length() <= 1){
            ValidationError error = new ValidationError();
            error.setCode("too_short_number");
            return error;
        }
        else return null;
    }
}
