package model;

import java.util.ArrayList;
import java.util.List;

public class ReportCreator {

    static final int TURNOVER_FINAL = 20;

    public ReportCreator(){}

    public static Report CreateReport(List<Order> inputOrders){
        List<Order> ordersList = new ArrayList<Order>();
        for(Order order : inputOrders){
            ordersList.add(order);
        }
        Report result = new Report();
        result.setCount(ordersList.size());
        int price = 0;
        for(Order order : ordersList){
            for(OrderRow row : order.getOrderRows()){
                price += row.getPrice() * row.getQuantity();
            }
        }
        result.setTurnoverVAT(TURNOVER_FINAL);
        result.setTurnoverWithoutVAT(price);
        if(inputOrders.size() > 0) result.setAverageOrderAmount(price / ordersList.size());
        else result.setAverageOrderAmount(price);
        result.setTurnoverWithVAT(price * (100 + TURNOVER_FINAL) / 100);
        return result;

    }
}
