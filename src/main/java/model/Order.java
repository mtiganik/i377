package model;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Order {

    private long id;
    private String orderNumber;
    private List<OrderRow> orderRows = new ArrayList<OrderRow>();

    public Order(){

    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderNumber='" + orderNumber + '\'' +
                ", orderRows=" + orderRows +
                '}';
    }

    public void addOrderRow(OrderRow orderRow) {
        if (orderRows == null) {
            orderRows = new ArrayList<>();
        }

        orderRows.add(orderRow);
    }

    public List<OrderRow> getOrderRows() {
        return orderRows;
    }

    public long getId(){ return this.id;}

    public String getOrderNumber(){ return this.orderNumber;}

    public void setId(long id){this.id = id;}

    public void setOrderNumber(String orderNumber){this.orderNumber = orderNumber;}


}
