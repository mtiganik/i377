package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderRow {
    private String itemName;
    private int quantity;
    private int price;

    public OrderRow(){}

    public OrderRow(String itemName, int quantity, int price){
        this.itemName = itemName;
        this.quantity = quantity;
        this.price = price;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public void setQuantity(int quantity){
        this.quantity = quantity;
    }

    public void setItemName(String itemName){
        this.itemName = itemName;
    }

    public String getItemName(){
        return itemName;
    }

    public int getQuantity(){
        return quantity;
    }

    public int getPrice(){
        return price;
    }

    @Override
    public String toString() {
        return "OrderRow{" +
                "itemName='" + itemName + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }


}
