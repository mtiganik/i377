package servlet;

import DAO.OrdersDAO;
import DB.DBCreator;
import Util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Order;
import model.ValidationError;
import model.ValidationErrorChecker;
import model.ValidationErrors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet("/api/orders")
public class OrderServlet extends HttpServlet {

    private OrdersDAO ordersDAO = new OrdersDAO();
    public static boolean hasDBBeenInitialized = false;


    private void initialize(){
        hasDBBeenInitialized = true;
        DBCreator.InitializeDatabase();
}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // NOT A GOOD PLACE FOR INITIALIZATION
        if(!hasDBBeenInitialized){
            initialize();
        }
        String input = Util.readStream(request.getInputStream());

            response.setContentType("application/json");
            System.out.println("Is valid JSON object");
            ObjectMapper objectMapper = new ObjectMapper();
            Order newOrder = objectMapper.readValue(input, Order.class);


            if(ValidationErrorChecker.CheckOrderErrors(newOrder) != null)
            {
                ValidationErrors errors = new ValidationErrors();
                errors.setErrors(Arrays.asList(ValidationErrorChecker.CheckOrderErrors(newOrder)));

                response.setStatus(400);
                response.getWriter().print(objectMapper.writeValueAsString(errors));
            }
            else{
                OrdersDAO ordersDao = new OrdersDAO();
                ordersDAO.saveOrder(newOrder);
                response.getWriter().print(objectMapper.writeValueAsString(newOrder));

            }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // NOT A GOOD PLACE FOR INITIALIZATION
        if(!hasDBBeenInitialized){
            initialize();
        }
        response.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        String idCode = request.getParameter("id");

        if(idCode != null){

            response.getWriter().print(objectMapper.writeValueAsString(ordersDAO.getOrderForId(Integer.parseInt(idCode))));
        }
        else {
            response.getWriter().print(objectMapper.writeValueAsString(ordersDAO.getAllOrders()));
        }
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("application/json");
        //response.getWriter().print("in doDelete method");

        String idCode = request.getParameter("id");
        if(idCode != null){
            ordersDAO.deleteOrder(Integer.parseInt(idCode));
            response.getWriter().print("order was deleted");
        }else{
            ordersDAO.deleteAll();
            response.getWriter().print("database was deleted");
        }
    }

}
