package servlet;

import DAO.OrdersDAO;
import DB.DBCreator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Order;
import model.Report;
import model.ReportCreator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/api/orders/report")
public class ReportServlet extends HttpServlet {
    private void initialize(){
        DBCreator.InitializeDatabase();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        // NOT A GOOD PLACE FOR INITIALIZATION
        if(!OrderServlet.hasDBBeenInitialized){
            OrderServlet.hasDBBeenInitialized = true;
            initialize();
        }


        response.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OrdersDAO orderDao = new OrdersDAO();

        System.out.println(orderDao.getAllOrders());

        List<Order> ordersList = orderDao.getAllOrders();
        Report report = ReportCreator.CreateReport(ordersList);
        System.out.println(objectMapper.writeValueAsString(report));

        response.getWriter().print(objectMapper.writeValueAsString(report));

        //response.getWriter().print("In report method");
    }
}
