package servlet;

import DAO.OrdersDAO;
import Util.Util;
import model.Order;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/orders/form")
public class FormServlet extends HttpServlet {

    private OrdersDAO ordersDAO = new OrdersDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String input = Util.readStream(request.getInputStream());

        if (input.contains("orderNumber")) {
            String[] orderNumber = input.split("=");
            System.out.println(orderNumber[1]);
            Order newOrder = new Order();
            newOrder.setOrderNumber(orderNumber[1]);

            ordersDAO.saveOrder(newOrder);
//            newOrder.setId(ordersDAO.getLastId());
            //Order.orders.add(newOrder);
            //response.getWriter().print(orderFromUrlEncoded.toString());

            response.getWriter().print(newOrder.getId());

            //ObjectMapper objectMapper = new ObjectMapper();

            //response.getWriter().print(objectMapper.writeValueAsString(orderFromUrlEncoded));

        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        String message = "Hello World";
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<body>");
        out.println("<h2>Enter new record</h2>");
        out.println("<form method='POST' enctype='application/x-www-form-urlencoded'>");
        out.println("Order number:<br>");
        out.println("<input type='Order number' name='orderNumber'>");
        out.println("<input type='submit' value='Submit'>");
        out.println("</form> ");
        out.println("</body>");
        out.println("</html>");

    }
}
