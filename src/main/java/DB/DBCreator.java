package DB;

import DAO.OrdersDAO;
import Util.FileUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Order;
import model.OrderRow;
import model.Report;
import model.ReportCreator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBCreator {


    public static void InitializeDatabase(){
        String schemasTemplate = FileUtil.readFileFromClasspath("schemas.sql");
        String dataTemplate =  FileUtil.readFileFromClasspath("data.sql");
        try (Connection conn = DriverManager.getConnection(OrdersDAO.URL);
             Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(schemasTemplate);
            stmt.executeUpdate(dataTemplate);
            //conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) throws JsonProcessingException {
        DBCreator.InitializeDatabase();

       OrdersDAO orderDAO = new OrdersDAO();
       Order o = new Order();
       o.setOrderNumber("K321");
       OrderRow or = new OrderRow();
       or.setItemName("Computer");
       or.setQuantity(2);
       or.setPrice(3);
       OrderRow or2 = new OrderRow();
       or2.setItemName("N321");
       or2.setPrice(3);
       o.addOrderRow(or2);
       o.addOrderRow(or);
       orderDAO.saveOrder(o);
        orderDAO.deleteOrder(1002);
        System.out.println(orderDAO.getAllOrders());
        orderDAO.deleteAll();
        orderDAO.saveOrder(o);
        orderDAO.saveOrder(o);
        System.out.println(orderDAO.getAllOrders());
       // System.out.println(orderDAO.getLastId());


        //System.out.println(orderDAO.getAllOrders());

        //Report report = ReportCreator.CreateReport(orderDAO.getAllOrders());

       // ObjectMapper mapper = new ObjectMapper();
       // System.out.println(mapper.writeValueAsString(report));
//
//        Order o3 = new Order();
//        o3.setOrderNumber("5");
//        orderDAO.saveOrder(o3);
//
//        orderDAO.deleteOrder(1004);
//
//        System.out.println(orderDAO.getAllOrders());
//        System.out.println("deleting:");
     //   orderDAO.deleteAll();

    }

}
