package DAO;

import model.Order;
import model.OrderRow;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrdersDAO {

    // public static String URL = "jdbc:hsqldb:file:${user.home}/data/jdbc/db;shutdown=true";

    public static String URL = "jdbc:hsqldb:mem:db1";

    public Order getOrderForId(int id) {

        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {
            Order o = null;

            try (ResultSet rset = stmt.executeQuery(
                    "SELECT o.id, o.orderRow, orw.id, orw.itemName, orw.quantity," +
                            " orw.price" +
                            "  FROM ORDERS o LEFT JOIN ORDERROWS orw ON o.id = orw.OrderId " +
                            "WHERE id = " + id + ";")) { // SQL injection... Prepared statement
                while (rset.next()) {
                    OrderRow or = new OrderRow();
                    or.setItemName(rset.getString(4));
                    or.setQuantity(rset.getInt(5));
                    or.setPrice(rset.getInt(6));

                    if (o == null) {
                        o = new Order();
                        o.setId(rset.getLong(1));
                        o.setOrderNumber(rset.getString(2));
                    }
                    o.addOrderRow(or);
                }
                return o;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteOrder(int id) {

        String sql = "DELETE FROM ORDERROWS WHERE OrderId = '" + id + "'; DELETE FROM ORDERS WHERE id = " + id;

        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        String sql = "TRUNCATE TABLE ORDERROWS; TRUNCATE TABLE ORDERS;";
        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<Order>();
        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {

            Order o = new Order();
            boolean newOrderFlag = true;
            try (ResultSet rset = stmt.executeQuery(
                    "SELECT o.id, o.orderRow, orw.id, orw.itemName, orw.quantity," +
                            " orw.price" +
                            "  FROM ORDERS o LEFT JOIN ORDERROWS orw ON o.id = orw.OrderId ORDER BY o.id")) {
                while (rset.next()) {

                    if (o.getId() != rset.getLong(1)) {
                        if (newOrderFlag) {
                            newOrderFlag = false;
                        } else {
                            orders.add(o);
                        }
                        o = new Order();
                        o.setId(rset.getLong(1));
                        o.setOrderNumber(rset.getString(2));
                    }

                    if (rset.getString(4) != null) {
                        OrderRow or = new OrderRow();
                        or.setItemName(rset.getString(4));
                        or.setPrice(rset.getInt(6));
                        or.setQuantity(rset.getInt(5));
                        o.addOrderRow(or);
                    }
                }
                if (o.getId() != 0) orders.add(o);
                return orders;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void saveOrder(Order order) {
        long generatedId = 0;

        String sql = "INSERT INTO ORDERS (id, orderRow) VALUES (NEXT VALUE FOR seq1, ? );";
        try (Connection conn = DriverManager.getConnection(URL);
             PreparedStatement ps =
                     conn.prepareStatement(sql, new String[]{"id"});
        ) {
            ps.setString(1, order.getOrderNumber());
            //ps.executeQuery();
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getLong("id");
                order.setId(generatedId);
            }
            if (!order.getOrderRows().isEmpty()) {
                saveOrderRows(order, conn);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveOrderRows(Order order, Connection conn) throws SQLException {
        for (OrderRow row : order.getOrderRows()) {

            String sql = "INSERT INTO ORDERROWS (id, OrderId, itemName, quantity, price) VALUES ( NEXT VALUE FOR seq2, ?,?,?,?);";
            try (
                    PreparedStatement ps = conn.prepareStatement(sql);
            ) {
                ps.setLong(1, order.getId());
                ps.setString(2, row.getItemName());
                ps.setInt(3, row.getQuantity());
                ps.setInt(4, row.getPrice());
                ps.executeUpdate();
            }
        }

    }
}